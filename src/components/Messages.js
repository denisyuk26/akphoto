import React, { useEffect } from "react";

import styled from "styled-components";

const Messages = props => {
  useEffect(() => {
    props.syncMessage();
  });

  const transformDate = date => {
    const hours = "0" + date.getHours();
    const minutes = "0" + date.getMinutes();
    const seconds = "0" + date.getSeconds();
    const dateOfMonth = "0" + date.getDate();
    const month = "0" + (date.getMonth() + 1);
    const year = date.getFullYear();
    return `${hours.slice(-2)}:${minutes.slice(-2)}:${seconds.slice(
      -2
    )} ${dateOfMonth.slice(-2)}/${month.slice(-2)}/${year}`;
  };

  const renderMessages = () =>
    props.messages.map(messageObj => {
      const { name, contacts, message, isNew, createdAt } = messageObj;
      const date = createdAt.toDate();
      const checkIsTrue = /true/gi.test(isNew);
      return (
        <StyledMessages newMessage={checkIsTrue} key={messageObj.id}>
          <StyledBlock>
            <StyledName>From: {name}</StyledName>
            <StyledStatus newMessage={checkIsTrue}>
              {!checkIsTrue ? "Readed" : "Not readed"}
            </StyledStatus>
          </StyledBlock>

          <StyledText>Contacts: {contacts}</StyledText>
          <StyledMessage>Message: {message}</StyledMessage>

          <StyledDate>{transformDate(date)}</StyledDate>

          {checkIsTrue ? (
            <StyledReadButton onClick={() => props.readMessage(messageObj)}>
              Checked
            </StyledReadButton>
          ) : null}
        </StyledMessages>
      );
    });

  return (
    props.isLogged && (
      <StyledWrap>{props.messages && renderMessages()}</StyledWrap>
    )
  );
};

const StyledReadButton = styled.button`
  position: sticky;
  top: 100%;
`;

const StyledMessages = styled.div`
  display: flex;
  position: relative;
  width: 100%;
  max-width: 25vw;
  justify-content: flex-start;
  flex-direction: column;
  flex-wrap: wrap;
  margin: 10px;
  background-color: ${props => (props.newMessage ? "#63ca5e" : "#f5f5f5")};
  color: ${props => (!props.newMessage ? "black" : "#f5f5f5")};
`;
const StyledWrap = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-around;
`;

const StyledText = styled.p`
  margin: 0 3px;
  padding: 0;
  font-size: 13px;
`;
const StyledName = styled(StyledText)`
  margin: 5px 3px;
  width: 75%;
`;
const StyledBlock = styled.div`
  display: flex;
  flex-direction: row;
  border-bottom: 0.5px solid #f1f1f1;
  margin: 3px 0;
  ${"" /* align-items: center; */} width: 100%;
`;
const StyledStatus = styled(StyledName)`
  width: 25%;
  text-align: right;
  color: ${props => (!props.newMessage ? "green" : "white")};
`;
const StyledDate = styled(StyledText)`
  margin: 5px 2px;
  text-align: right;
`;
const StyledMessage = styled(StyledText)`
  font-size: 14px;
  margin-top: 5px;
`;


export default Messages;
