import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const ModalWindow = styled.div`
	left: 0;
	right: 0;
	height: 100vh;
	position: fixed;
	top: 0;
	bottom: 0;
	background: rgba(10, 10, 10, .8);
	overflow: none;
	display: flex;
`;

const FullSizeImg = styled.img`
	height: 80vh;
	max-width: 90%;
	margin: 0 auto;
	padding-top: 40px;
`;
const CloseButton = styled.p`
	color: #fff;
	padding: 10px;
	outline: none;
	border: none;
	background: red;
	width: 60px;
	border-radius: 5px;
	right: 25px;
	position: absolute;
	cursor: pointer;
	text-align: center;
`;

const NavigationButton = styled.button`
	border: none;
	outline: none;
	width: 5vw;
	height: 90vh;
	position: fixed;
	color: #fff;
	background: #3f3f3f;
	font-size: 6em;
	&:hover {
		color: #6f6f6f;
	}
`;

const NextButton = styled(NavigationButton)`
	top: 7%;
	right: 0;
`;
const PrevButton = styled(NavigationButton)`
	top: 7%;
	left: 0;
`;
const Modal = (props) => {
	const [ url, setUrl ] = useState(props.item);
	const index = props.items.findIndex((i) => url === i.url);
	const nextPhoto = () => (index >= props.items.length - 1 ? false : setUrl(props.items[index + 1].url));
	const prevPhoto = () => (index <= 0 ? false : setUrl(props.items[index - 1].url));
	const arrowNavigation = ({ key }) => {
		switch (key) {
			case 'ArrowRight':
				return index >= props.items.length - 1 ? false : setUrl(props.items[index + 1].url);
			case 'ArrowLeft':
				return index <= 0 ? false : setUrl(props.items[index - 1].url);
			default:
				break;
		}
	};
	useEffect(() => {
		document.addEventListener('keydown', arrowNavigation);
		return () => {
			document.removeEventListener('keydown', arrowNavigation);
		};
	});
	return (
		<ModalWindow>
			<CloseButton onClick={() => props.closeModal(false)}>Close</CloseButton>
			<FullSizeImg src={url} />
			<PrevButton onClick={() => prevPhoto()}>&#8249;</PrevButton>
			<NextButton onClick={() => nextPhoto()}>&#8250;</NextButton>
		</ModalWindow>
	);
};

Modal.propTypes = {
	items: PropTypes.array,
	item: PropTypes.string,
	closeModal: PropTypes.func
};

export default Modal;
