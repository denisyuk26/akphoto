import React from 'react';
import { hot } from 'react-hot-loader';
import { Route, Switch } from 'react-router-dom';
import styled from 'styled-components';

import Loader from './Loader';
import AuthHeader from './AuthHeader';
import Header from './Header';
import About from './About';
import Contacts from '../containers/contacts.container';
import Gallery from '../containers/gallery.container';
import Admin from '../containers/admin.container';
import Messages from '../containers/message.container';
import Album from '../containers/album.container'
import PrivateRoute from './PrivateRoute'


const StyledApp = styled.div`
	font-family: "Oswald", sans-serif;
	width: 100%;
	`
const StyledWrapper = styled.div`
	display: flex;
	flex-wrap: wrap;
	align-items: center;
	min-height: 75vh;
	justify-content: center;
`;
function App(props) {
	return (
		<StyledApp>
			{props.status === 'request' ? (
				<Loader />
			) : (
				<StyledWrapper>
					{props.isLogged ? <AuthHeader messages={props.messages} logOut={props.logOut} /> : <Header />}
					<Switch>
						<Route exact path="/" component={About} />
						<Route exact path="/contacts" component={Contacts} />
						<Route exact path="/photos" component={Gallery} />
						<Route exact path="/photos/:name" component={Album} />
						<Route exact path="/admin" component={Admin} />
						<PrivateRoute exact path="/messages" isLogged={props.isLogged} component={Messages} />
					</Switch>
				</StyledWrapper>
			)}
		</StyledApp>
	);
}

export default hot(module)(App);
