import React, { useEffect } from 'react';
import styled from 'styled-components';
import UserAlbumList from './UserAlbumList'

const GaleryContainer = styled.div`
	width: 100%;
	display: flex;
	justify-content: space-around;
	flex-wrap: wrap;
	flex-direction: row;
`;

const StyledTitle = styled.h3`
	width: 100%;
	text-align: center;
`;
const Galery = (props) => {
	useEffect(() => {
		props.syncPhotos();
	}, [props.photos]);
	return (
		<GaleryContainer>
            <StyledTitle>Albums</StyledTitle>
            <UserAlbumList path={props.history.location.pathname} select={props.selectAlbum} albums={props.photos} sync={props.syncPhotos}/>
		</GaleryContainer>
	);
};


export default Galery;
