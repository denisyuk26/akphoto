import React from 'react';
import styled, { keyframes } from 'styled-components';

const keyframesLoader = keyframes`  
    0 {transform: translate(0,0);}
    50% {transform: translate(0,15px);}
    100% {transform: translate(0,0);}
`;

const StyledLoader = styled.div`
	height: 100vh;
	width: 100vw;
	display: flex;
	justify-content: center;
	align-items: center;
	div:nth-last-child(1) {
		animation: ${keyframesLoader} .6s .1s linear infinite;
	}
	div:nth-last-child(2) {
		animation: ${keyframesLoader} .6s .2s linear infinite;
	}
	div:nth-last-child(3) {
		animation: ${keyframesLoader} .6s .3s linear infinite;
	}
`;
const Line = styled.div`
	display: inline-block;
	width: 25px;
	height: 25px;
	border-radius: 30px;
	background-color: #edbc97;
	margin: 5px;
	${'' /* animation: ${keyframesLoader} .6s .1s linear infinite; */};
`;
const Loader = (props) => (
	<StyledLoader>
		<Line />
		<Line />
		<Line />
	</StyledLoader>
);

export default Loader;
