import React, { useState, useEffect, Fragment } from "react";
import { useDropzone } from "react-dropzone";
import styled from "styled-components";

const Container = styled.div`
  display: flex;
  justify-content: center;
  margin: 0 auto;
  width: 50vw;
  height: 20vh;
  background: #f5d1b6;
  &:hover {
    background: #e1e1e1;
  }
`;
const InputForUpload = styled.input`
  width: 100%;
`;
const StyledText = styled.p`
  color: white;
  align-self: center;
`;
const ProgressLine = styled.progress`
  -webkit-appearance: none;
  appearance: none;
  margin: 0;
  padding: 0;
  width: 100%;

  ::-webkit-progress-bar {
    background-color: #A7A7A7;
  }

  ::-webkit-progress-value {
    background-color: #55a942;
  }
`;

const PreviewImg = styled.img`
  width: 150px;
  height: 140px;
  margin: 10px;
`;

const PreviewContainer = styled(Container)`
  height: 15vw;
  background: rgba(0, 0, 0, 0);
  &:hover {
    background: none;
  }
`;

const StyledButton = styled.button`
  font-family: "Oswald", sans-serif;
  width: 35%;
  color: #a7a7a7;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 20px;
  margin: 0 0 0 45%;
  font-size: 16px;
  &:hover {
    background: #f59e5d;
    color: #fff;
  }
`;
const StyledWrap = styled.div`
  margin-top: -64px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const StyledButtom = styled.button`
  font-family: "Oswald", sans-serif;
  align-self: ${props => props.button === 'back' ? 'flex-start' : 'space-between'};
  color: #a7a7a7;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 20px;
  margin: ${props => props.button === 'back' ? '0' : '0 auto'};
  font-size: 16px;
  &:hover {
    background: ${props => (props.button === "upload" ? "#55a942" : "#f59e5d")};
    color: #fff;
  }
`;
function MyDropzone(props) {
  const [files, setFiles] = useState([]);
  const [previewIMG, setPreviewIMG] = useState([]);
  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop: acceptedFiles => {
      let preview = acceptedFiles.map((file, id) => ({
        preview: URL.createObjectURL(file),
        id: file.name + parseInt(Math.random(0, 1) * 100)
      }));
      setFiles(files.concat(acceptedFiles));
      setPreviewIMG(previewIMG.concat(preview));
    },
    accept: "image/*"
  });
  const currentAlbum = props.albums.find(i => i.name === props.album);

  const uploadFile = files => {
    if (files.length > 0) {
      props.uploadPhoto(files);
    }
  };
  useEffect(() => {
    return () => {
      // Make sure to revoke the data uris to avoid memory leaks
      previewIMG.forEach(file => URL.revokeObjectURL(previewIMG.preview));
    };
  });
  console.log(files);
  const updateAlbum = () => {
    setPreviewIMG([]);
    props.updateAlbum();
    setFiles([]);
  };

  const goBack = () => {
    props.selectAlbum("");
    setPreviewIMG([]);
    setFiles([]);
    // setAlbumName("");
  };

  return (
    <Fragment>
      {props.album !== "" ? (
        <StyledWrap>
          <div style={{ height: "7vh", width: "80%", alignSelf: 'flex-start', flexDirection: 'row', display: 'flex' }}>
            {props.percentage === 100 ? (
              <StyledButton  button='public' onClick={() => updateAlbum()}>public </StyledButton>
            ) : (
              <>
                <StyledButtom  button='back' onClick={() => goBack()}>go back</StyledButtom>
                {files.length > 0 ? (
                  <StyledButtom button='upload' onClick={() => uploadFile(files)}>
                    upload Files
                  </StyledButtom>
                ) : null}
              </>
            )}
          </div>
          <div>
            <p>
              Album {currentAlbum && currentAlbum.name} has{" "}
              {currentAlbum && currentAlbum.photos.length} photos
            </p>
            <ProgressLine value={props.percentage} max="100" id="uploader">
              0%
            </ProgressLine>
            <Container {...getRootProps()}>
              <InputForUpload {...getInputProps()} />

              {isDragActive ? (
                <StyledText>Drag 'n' drop some files here</StyledText>
              ) : (
                <StyledText>Drag or Click to upload photos</StyledText>
              )}
            </Container>
            <PreviewContainer>
              {previewIMG.map(url => {
                return <PreviewImg key={url.id} src={url.preview} />;
              })}
            </PreviewContainer>
          </div>
        </StyledWrap>
      ) : null}
    </Fragment>
  );
}

export default MyDropzone;
