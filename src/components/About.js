import React from 'react';
// import PropTypes from 'prop-types';
import styled from 'styled-components';
import { hot } from 'react-hot-loader';

const AboutContainer = styled.div`
	display: flex;
	justify-content: center;
	background: #f5f5f5;
`;
const About = (props) => {
	return (
		<AboutContainer>
			<div>
				<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. </p>
				<p>
					Quaerat atque consectetur, unde eligendi nesciunt vel magni quos, doloremque voluptatum quidem quas
					hic, eveniet nostrum?{' '}
				</p>
				<p>Suscipit quam tempora molestiae atquse sequi ipsam, ducimus similique.</p>
				<p>
					{' '}
					Repudiandae, labore omnis doloremque est id temporibus ullam laborum sit laudantium dolore, debitis
					dolorum aliquid ducimus eius.
				</p>
			</div>
		</AboutContainer>
	);
};

// About.propTypes = {};

export default hot(module)(About);
