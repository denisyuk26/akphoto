import React, { useState } from 'react';
// import PropTypes from 'prop-types';
import styled from 'styled-components';
import { FaFacebook, FaInstagram } from 'react-icons/fa';
import { Link } from 'react-router-dom';
const StyledContactsWrap = styled.div`
	width: 100%;
	display: flex;
	min-height: 50vh;
	flex-wrap: wrap;
`;
const StyledForm = styled.form`
	display: flex;
	width: 100%;
	justify-content: center;
	align-items: center;
	flex-direction: column;
`;

const StyledInput = styled.input`
	margin-bottom: 10px;
	max-width: 30.2vw;
	min-height: 40px;
	width: 100%;
	border: none;
	outline: none;
	background: #f2f1f1;
	padding: 0 25px;
`;
const StyledNameInput = styled(StyledInput)`
	width: 100%;

`;
const StyledTextArea = styled.textarea`
	max-width: 30vw;
	background: #f2f1f1;
	border: none;
	width: 100%;
	padding: 10px 25px;
`;
const StyledContactsInput = styled(StyledInput)`
`;
const StyledButton = styled.button`
	outline: none;
	border: none;
	width: 10vw;
	padding: 10px;
	margin-top: 5px;
	&:hover {
		background: green;
		color: #fff;
	}
`;

const StyledMessageNotification = styled.div`
	color: green;
	margin: 10px 0;
	align-self: center;
	text-align: center;
	width: 100%;
`;
const StyledBlock = styled.div`
	display: flex;
	flex-direction: row;
	justify-content: center;
	font-size: 15px;
	margin: 5px 10px;
	padding: 0;
	width: 100%;
	max-height: 30px;
`;

const StyledText = styled.p`
	font-size: 40px;
	font-weight: 400;
	margin: 7px 10px;
`;
const StyledTitle = styled.h3`
	text-align: center;
	width: 100%;
`;
const StyledInfo = styled(StyledText)`
    width: 100%;
    text-align: center;
	font-size: 16px;
`;

const StyledFacebook = styled(FaFacebook)`
	color: #3b5998
`;

const StyledInstagram = styled(StyledText)`
	& > svg * {
		fill: url(#rg);
	}
`;

const Contacts = (props) => {
	const [ userInput, setUserInput ] = useState({
		name: '',
		phone: '',
		message: '',
		valid: false
	});
	const checkIsValid = ({ name, phone, message }) => {
		const isEmpty = (name && phone && message) === '';
		const isString = typeof (name && phone && message) === 'string';
		const isVerifedPhone = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/;
		if (isString && !isEmpty && isVerifedPhone.test(phone)) {
			setUserInput({ ...userInput, valid: true });
			props.sendMessage({ name, phone, message });
		}
		setUserInput({ name: '', phone: '', message: '', valid: false });
	};

	return (
		<StyledContactsWrap>
			<StyledTitle>Contact me</StyledTitle>
			{props.sendStatus === 'success' && (
				<StyledMessageNotification> Your message has been sent. </StyledMessageNotification>
			)}
			<StyledInfo>
				If you have any question, you can send it through the form below or you can contact with me following
				links on this page.
			</StyledInfo>
			<StyledInfo> I would be glad to give answer on your questions.</StyledInfo>
			<StyledBlock>
				<Link to="/" target="blank">
					<StyledText>
						<StyledFacebook />
					</StyledText>
				</Link>
				<Link to="/" target="blank">
					<StyledInstagram>
						<FaInstagram />
					</StyledInstagram>
				</Link>
			</StyledBlock>
			<StyledForm onSubmit={(e) => e.preventDefault()}>
				<p> </p>
				<StyledNameInput
					value={userInput.name}
					onChange={(e) =>
						setUserInput({
							...userInput,
							name: e.target.value
						})}
					placeholder="Enter your name*"
					type="text"
				/>
				<StyledContactsInput
					value={userInput.phone}
					onChange={(e) =>
						setUserInput({
							...userInput,
							phone: e.target.value
						})}
					placeholder="Enter your phone*"
					type="text"
				/>

				<StyledTextArea
					value={userInput.message}
					onChange={(e) =>
						setUserInput({
							...userInput,
							message: e.target.value
						})}
					placeholder="Message here*"
					cols="30"
					rows="5"
				/>
				<StyledButton onClick={() => checkIsValid(userInput)}>Send</StyledButton>
			</StyledForm>
			<svg width="0" height="0">
				<radialGradient id="rg" r="150%" cx="30%" cy="107%">
					<stop stopColor="#fdf497" offset="0" />
					<stop stopColor="#fdf497" offset="0.05" />
					<stop stopColor="#fd5949" offset="0.45" />
					<stop stopColor="#d6249f" offset="0.6" />
					<stop stopColor="#285AEB" offset="0.9" />
				</radialGradient>
			</svg>
		</StyledContactsWrap>
	);
};

// Contacts.propTypes = {};

export default Contacts;
