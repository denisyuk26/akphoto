import React from 'react';
// import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

const LinksWrap = styled.ul`
font-family: 'Oswald', sans-serif;

	list-style-type: none;
	display: flex;
	padding-inline-start: 0;
	justify-content: space-evenly;
	width: 100%;
	margin: 0;
`;
const StyledLink = styled.li`margin: 5px 25px; font-size: 20px;`;
const LinkText = styled(Link)`
	color: #4f4f4f;
	text-decoration: none;
	&:visited {
		color: #fff;
	}
	&:hover {
		color: #444444;
	}
`;
const LinksList = (props) => {
	return (
		<LinksWrap auth={props.auth}>
			<StyledLink>
				<LinkText to="/">About</LinkText>
			</StyledLink>
			<StyledLink>
				<LinkText to="/photos">Galery</LinkText>
			</StyledLink>
			<StyledLink>
				<LinkText to="/contacts">Contacts</LinkText>
			</StyledLink>
			{/* <StyledLink>
				<LinkText to="/admin">Admin</LinkText>
			</StyledLink> */}
		</LinksWrap>
	);
};

// LinksList.propTypes = {};

export default LinksList;
