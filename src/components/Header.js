import React from 'react';
// import PropTypes from 'prop-types';
import styled from 'styled-components';
import { hot } from 'react-hot-loader';
import { Link } from 'react-router-dom';
import LinksList from './LinksList';

const StyledHeader = styled.header`
font-family: 'Sacramento', cursive;
	display: flex;
	flex-direction: row;
	flex-wrap: wrap;
	justify-content: center;
	width: 100%;
	align-self: flex-start;
	background: #edbc97;
	font-size: 40px;
`;

const StyledTitleBlock = styled(Link)`
	align-self: center;
	text-decoration: none;
	text-align: left;
	width: 100%%;
	&:visited {
		color: #000000;
	}
`;

const StyledText = styled.p`
	
	margin: 25px 0 0 0;
	font-size: 50px;
	color: #fff;
	align-self: center
	&:hover {
		color: #7f7f7f;
	}
`;
const Header = (props) => {
	return (
		<StyledHeader>
			<StyledTitleBlock to="/">
				<StyledText>Kuzmichevskaya Photography</StyledText>
			</StyledTitleBlock>
			<LinksList auth={false} />
		</StyledHeader>
	);
};

// Header.propTypes = {};

export default hot(module)(Header);
