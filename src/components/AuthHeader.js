import React, { Fragment, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';

import LinksList from './LinksList';
import styled from 'styled-components';
import { FaEnvelope, FaSignOutAlt } from 'react-icons/fa';

const StyledHeader = styled.header`
	display: flex;
	width: 100%;
	padding: 10px 0;
	flex-wrap: nowrap;
	justify-content: flex-end;
	background: #edbc97;
	align-self: flex-start;
`;



const StyledLink = styled(Link)`
	color: #fff;
	text-decoration: none;
	text-transform: none;
`;
const StyledAnnounce = styled.div`
	position: relative;
	color: #fff;
	align-self: center;
	margin-right: 15px;
	display: flex;
	align-items: center;
`;
const StyledCount = styled.span`
    position: absolute;
    left: 20px;
	border-radius: 50%;
	padding: 1px 8px;
	background: ${(props) => (props.count > 0 ? 'red' : 'transparent')};
`;

const StyledLogOut = styled(FaSignOutAlt)`
	align-self: center;
	padding: 10px;
	color: #fff;
	border: none;
	border-radius: 5px;
	max-width: 9vw;
	cursor: pointer;
	outline: none;
	font-size: 30px;
	&:hover {
		color: #ea6060;
	}
`;
const StyledFa = styled(FaEnvelope)`
	color: #fff;
	font-size: 24px;
	align-self: center;
	vertical-align: middle;
	margin: 0 3px 3px 0;	
	cursor: pointer;
`;

const AuthHeader = (props)=> {
	const [ countMessages, setCountMessages ] = useState(0);
	const filterNewMessages = (data) => {
		if (data) return data.filter((item) => item.isNew === 'true');
	};
	useEffect(
		() => {
			if (props.messages) {
				const newMessage = filterNewMessages(props.messages);
				setCountMessages(newMessage.length);
			}
		},
		[ props.messages ]
	);

	const logOut =() => {
		const {history} = props;
		props.logOut();
		history.push("/")
	}
	return (
		<Fragment>
			<StyledHeader>
				<LinksList auth={true} />
				<StyledAnnounce>
					<StyledLink to="/messages">
						<StyledFa />
						<StyledCount count={countMessages}>{countMessages}</StyledCount>
					</StyledLink>
				</StyledAnnounce>
				<StyledLogOut onClick={() => logOut()} />
			</StyledHeader>
		</Fragment>
	);
};

AuthHeader.propTypes = {
	logOut: PropTypes.func
};

export default withRouter(AuthHeader);
