import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

const StyledContainer = styled.div`
  margin-top: 15px;
  max-width: 80vw;
  width: 100%;
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-auto-rows: minmax(0, auto);
`;
const Photo = styled.img`
  max-width: 80%;
  width: 100%;
  margin: 10px;
  cursor: pointer;
`;

const StyledTitle = styled.h3`
  width: 100%;
  text-align: center;
  font-weight: 400;
`;

const StyledAlbumContainer = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  margin: 0 auto;
`;

const StyledLink = styled(Link)`
  color: #000;
  text-align: center;
  text-decoration: none;
  text-transform: none;
`;
const UserAlbumList = props => {
  const memoAlbums = () => {
    return props.albums.map(item => {
      if (item) {
        const name = item.name.replace(/ /g, "-");
        return (
          <StyledAlbumContainer key={name}>
            <StyledLink
              onClick={() => props.select(item.name)}
              to={`${props.path}/${name}`}
            >
              <Photo
                loading="lazy"
                src={item.photos[0].path}
                alt={item.photos[0].name}
              />
              <StyledTitle>{item.name}</StyledTitle>
            </StyledLink>
          </StyledAlbumContainer>
        );
      }
      return item
    });
  };

  return <StyledContainer>{memoAlbums()}</StyledContainer>;
};

export default UserAlbumList;
