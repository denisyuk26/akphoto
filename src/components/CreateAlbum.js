import React, { useState, Fragment } from "react";

import styled from "styled-components";


const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin: 0 auto;
`;
const StyledInput = styled.input`
  width: 100%;
  padding: 15px 0 2px 0;
  border: none;
  outline: none;
  border-bottom: 1px solid #a7a7a7;
  text-align: center;
  &::-webkit-input-placeholder {
    font-style: italic;
  }
`;
const StyledText = styled.p`
  padding: 20px;
  text-align: center;
  height: 20px;
  font-size: 18px;
  margin: 0;
`;
const StyledLabel = styled.label`
  align-self: center;
  margin: 15px;
`;

const StyledButtom = styled.button`
  font-family: "Oswald", sans-serif;
  display: ${props=> props.status !== 'success' ? 'block' : 'none'};
  color: #a7a7a7;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 20px;
  align-self: ${props => (props.button === "delete" ? "flex-end" : "center")};
  margin: ${props => (props.button === "delete" ? "0 5px" : "0 auto")};
  font-size: 16px;
  &:hover {
    background: ${props => (props.button === "create" ? "#f59e5d" : "#ea6060")};
    color: #fff;
  }
`;
const StyledButtonsContainer = styled.div`
  align-items: center;
  display: flex;
  justify-content: space-between;
  flex-direction: column;
`;
const CreateAlbum = props => {
  const [albumName, setAlbumName] = useState(props.album);
  const createAlbum = albumName => {
    setAlbumName("");
    return albumName !== "" ? props.create(albumName) : "";
  };
  const deleteAlbum = text => props.deleteAlbum(text);
  return (
    <Fragment>
     
      <Container>
        {props.album === "" ? (
          <Fragment>
            <StyledLabel htmlFor="albumName">
              Enter name for your album
            </StyledLabel>
            <StyledInput
              type="text"
              onChange={e => setAlbumName(e.target.value)}
              value={albumName}
              placeholder="Album name"
              id="albumName"
            />
            <StyledText>{albumName}</StyledText>
          </Fragment>
        ) : null}

        <StyledButtonsContainer>
          {props.album !== "" ? (
            <StyledButtom
              status={props.status}
              button="delete"
              onClick={() => deleteAlbum(albumName)}
            >
              delete album{" "}
            </StyledButtom>
          ) : (
            <StyledButtom
              button="create"
              onClick={() => createAlbum(albumName)}
            >
              Create album
            </StyledButtom>
          )}
        </StyledButtonsContainer>
      </Container>
    </Fragment>
  );
};

export default CreateAlbum;
