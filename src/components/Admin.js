import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { hot } from "react-hot-loader";
import AlbumsList from "./AlbumsList";
import LogInForm from "./LogInForm";
import MyDropzone from "./DropPhotos";
import CreateAlbum from "./CreateAlbum";
const AdminWrap = styled.div`
  width: 100%;
  min-height: 80vh;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

const Admin = props => {
  return (
    <AdminWrap>
      {props.isLogged ? (
        <div style={{ width: "50vw" }}>
         

          <CreateAlbum
            create={props.createAlbum}
            deleteAlbum={props.deleteAlbum}
            album={props.album}
            albums={props.albums}
            selectAlbum={props.selectAlbum}
            syncDoc={props.syncDoc}
            status={props.uploadStatus}
          />
          <MyDropzone
            uploadPhoto={props.uploadPhoto}
            updateAlbum={props.updateAlbum}
            albums={props.albums}
            photoName={props.photoName}
            percentage={props.percentage}
            album={props.album}
            selectAlbum={props.selectAlbum}
          />
           <AlbumsList
            albums={props.albums}
            album={props.album}
            selectAlbum={props.selectAlbum}
            syncDoc={props.syncDoc}
          />
        </div>
      ) : (
        <LogInForm logIn={props.logIn} />
      )}
    </AdminWrap>
  );
};

Admin.propTypes = {
  isLogged: PropTypes.bool
};

export default hot(module)(Admin);
