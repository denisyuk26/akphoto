import React, { useEffect } from "react";

import styled from "styled-components";

const StyledContainer = styled.div`
  margin-top: 15px;
  max-width: 80vw;
  width: 100%;
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-auto-rows: minmax(0, auto);
  justify-items: center
 
`;
const Photo = styled.img`
  width: 98%;
  min-height: 70vh;
  margin: 10px;
  &:nth-child(3n) {
    margin: 0px;
    width: 99%;
    grid-column: 1 / 3;
    }
`;

const StyledTitle = styled.p`
  width: 100%;
  font-family: "Oswald",sans-serif;
  text-align: center;
  font-weight: normal;
  font-size: 36px;
`;


const Album = props => {
  useEffect(() => {
    if (props.alubmName === "") {
      props.syncDoc();
      getDataOnPageUpdate();
    }
  });

  const getDataOnPageUpdate = () => {
    const path = props.history.location.pathname;
    const albumName = path.replace(/\/photos\//gi, "").replace(/-/gi, " ");
    return props.selectAlbum(albumName);
  };
  return (
    <div>
      <StyledTitle>{props.album.name}</StyledTitle>
      {props.album.photos !== undefined
        ? <StyledContainer>
       { props.album.photos.map(item => {
            return (
              
                <Photo key={item.name} src={item.path} alt={item.name} />
              
            );
          })}
        </StyledContainer>
        : null}
    </div>
  );
};


export default Album;
