import React from "react";
import styled from "styled-components";

const StyledContainer = styled.div`
  width: 100%;
`;
const StyledImg = styled.img`
  width: 80px;
  height: 80px;
  margin: 5px;
`;

const AlbumListWrap = styled.div`
  display: flex;
  width: 100%;
  ${"" /* border: 1px solid black; */}
  padding: 5px;
  margin: 5px 0;
  flex-wrap: wrap;
  flex-direction: row;
  background: #edbc97;
  color: #fff;
  justify-content: space-around;
  align-items: center;
  cursor: pointer;
  &:hover {
    background: #f59e5d;
  }
`;

const StyledTextWrap = styled.div`
  width: 80%;
  display: flex;
  justify-content: space-evenly;
`;

const StyledTitle = styled.h3`
  text-align: center;
  font-weight: 300;
`
const AlbumsList = props => {
  const selectAlbum = item => {
    props.selectAlbum(item);
    props.syncDoc(item);
  };
  return (
    <StyledContainer>
      {props.album === ""
        ? <div>
      <StyledTitle>Existed albums </StyledTitle>
          {
            props.albums.map(item => {
            console.log(item);

            return (
              <AlbumListWrap
                onClick={() => selectAlbum(item.name)}
                key={item.name}
              >
                {item.photos[0] && <StyledImg src={item.photos[0].path} />}

                <StyledTextWrap>
                  <p>
                    <i>Album:</i> {item.name}
                  </p>
                  <p>{item.photos.length} photos</p>
                </StyledTextWrap>
              </AlbumListWrap>
            );
          })
          }
        </div>
        : null}
    </StyledContainer>
  );
};

export default AlbumsList;
