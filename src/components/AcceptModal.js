import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const ModalWrap = styled.div`
	position: fixed;
	display: flex;
	width: 30vw;
	min-height: 20vh;
	flex-direction: column;
	background: rgba(10, 10, 10, .8);
	justify-content: center;
	align-items: center;
`;
const Text = styled.p`
	color: #fff;
	text-align: center;
`;
const AcceptButton = styled.p`
	outline: none;
	background-color: #2aa745;
	padding: 15px;
	cursor: pointer;
	margin: 5px;
	padding: 10px 35px;

	color: #fff;
	border: none;
	border-radius: 5px;
	max-width: 15vw;
	width: 100%;
`;

const CancelButton = styled(AcceptButton)`
	background-color: red;

`;

const ButtonWrap = styled.div`display: flex;`;

const AcceptModal = (props) => {
	const acceptDelete = () => {
		props.delete(props.item);
		props.accept(false);
	};
	return (
		<ModalWrap>
			<Text>Delete {props.item.name}?</Text>
			<ButtonWrap>
				<AcceptButton onClick={() => acceptDelete()}> Yes</AcceptButton>
				<CancelButton onClick={() => props.accept(false)}> No</CancelButton>
			</ButtonWrap>
		</ModalWrap>
	);
};

AcceptModal.propTypes = {
	item: PropTypes.object,
	accept: PropTypes.func
};

export default AcceptModal;
