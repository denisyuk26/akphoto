import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { hot } from 'react-hot-loader';

const LogInForm = (props) => {
	const [ userInput, setUserInput ] = useState({
		login: '',
		password: ''
	});

	return (
		<StyledForm htmlFor="send">
			<StyledInput
				onChange={({ target: { value } }) => setUserInput({ ...userInput, login: value })}
				type="text"
			/>
			<StyledInput
				type="password"
				onChange={({ target: { value } }) => setUserInput({ ...userInput, password: value })}
			/>
			<StyledButton id="send" type="button" onClick={() => props.logIn(userInput)}>
				Log In
			</StyledButton>
		</StyledForm>
	);
};

const StyledForm = styled.form`
	display: flex;
	max-width: 20vw;
	margin: 0 auto;
	flex-direction: column;
	justify-content: center;
	align-items: center;
`;
const StyledInput = styled.input`
	padding: 5px;
	heigth: 20px;
`;
const StyledButton = styled.button`
	padding: 5px;
	heigth: 20px;
	background: blue;
	outline: none;
	border: none;
	color: #ffffff;
`;

LogInForm.propTypes = {
	logIn: PropTypes.func
};

export default hot(module)(LogInForm);
