import { fork, all } from 'redux-saga/effects';
import { AlbumWatcher, uploadPhotoWatcher, sendPhotoUrlWatcher, getPhotoWatcher, messagesWatcher } from './adminWatcher.saga';
import { syncPhotoSaga, syncDocSaga } from '../actions/admin.action';
import { loginRootSaga } from '../actions/login';
import { sagaOnSyncMessages } from '../actions/message.action';

export default function* rootSaga() {
	yield all([
		fork(AlbumWatcher),
		fork(syncDocSaga),
		fork(loginRootSaga),
		fork(syncPhotoSaga),
		fork(messagesWatcher),
		fork(sagaOnSyncMessages),
		fork(uploadPhotoWatcher),
		fork(sendPhotoUrlWatcher),
		fork(getPhotoWatcher)
	]);
}
