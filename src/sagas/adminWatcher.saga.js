import { all, takeLatest, takeEvery } from 'redux-saga/effects';
import { uploadPhoto, sagaOnUploadPhoto, sendPhotoUrl, deletePhoto, sagaOnDeletePhoto, createAlbum, sagaOnCreateAlbum, sagaOnUpdateAlbums, updateAlbum, deleteAlbum, sagaOnDeleteAlbum, selectAlbum, sagaOnSelectAlbum } from '../actions/admin.action';
import { sendMessage, sagaOnSendMessage, sagaOnReadMessage, readMessage } from '../actions/message.action';


export function* uploadPhotoWatcher() {
	yield all([ yield takeEvery(uploadPhoto.TRIGGER, sagaOnUploadPhoto) ]);
}



export function* getPhotoWatcher() {
	yield takeLatest(deletePhoto.TRIGGER, sagaOnDeletePhoto);
}
export function* sendPhotoUrlWatcher() {
	yield takeEvery(sendPhotoUrl.TRIGGER, sendPhotoUrl);
}

export function* AlbumWatcher() {
	yield all([
		yield takeEvery(createAlbum.TRIGGER, sagaOnCreateAlbum),
		yield takeEvery(deleteAlbum.TRIGGER, sagaOnDeleteAlbum),
		yield takeLatest(updateAlbum.TRIGGER, sagaOnUpdateAlbums),
		yield takeLatest(selectAlbum.TRIGGER, sagaOnSelectAlbum)

	]);
}
export function* messagesWatcher() {
	yield all([
		yield takeEvery(sendMessage.TRIGGER, sagaOnSendMessage),
		yield takeLatest(readMessage.TRIGGER, sagaOnReadMessage)
	]);
}
