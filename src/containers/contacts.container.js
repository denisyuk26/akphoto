import Contacts from '../components/Contacts';
import { connect } from 'react-redux';
import { sendMessage } from '../actions/message.action';
const mapStateToProps = (state) => ({
	isLogged: state.adminReducer.logInData.logInStatus.isLogged,	
	status: state.adminReducer.logInStatus,
	sendStatus: state.adminReducer.messageStatus
});

export default connect(mapStateToProps, { sendMessage })(Contacts);
