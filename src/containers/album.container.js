import Album from '../components/Album';
import { selectAlbum, syncDoc } from '../actions/admin.action';
import { connect } from 'react-redux';
const mapStateToProps = (state) => ({
    album: state.adminReducer.albumData,
    alubmName: state.adminReducer.album
});

export default connect(mapStateToProps, { syncDoc, selectAlbum })(Album);
