import App from '../components/App';
import { syncPhotos } from '../actions/admin.action';
import { connect } from 'react-redux';
import { logOut } from '../actions/login';
import { sendMessage, syncMessage } from '../actions/message.action';
const mapStateToProps = (state) => ({
	isLogged: state.adminReducer.logInData.logInStatus.isLogged,
	status: state.adminReducer.logInData.logInStatus.status,
	messages: state.adminReducer.syncMessages.data
});

export default connect(mapStateToProps, { logOut, syncPhotos, sendMessage, syncMessage })(App);
