import { connect } from "react-redux";


import Admin from "../components/Admin";
import {
  createAlbum,
  uploadPhoto,
  sendPhotoUrl,
  updateAlbum,
  deleteAlbum,
  selectAlbum,
  syncDoc
} from "../actions/admin.action";
import { logIn, logOut } from "../actions/login";



const mapStateToProps = state => ({
  loginStatus: state.adminReducer.logInStatus,
  isLogged: state.adminReducer.logInData.logInStatus.isLogged,
  percentage: state.adminReducer.uploadStatus.percentage,
  uploadStatus: state.adminReducer.uploadStatus.status,
  album: state.adminReducer.album,
  albums: state.adminReducer.syncPhoto
});

export default connect(
  mapStateToProps,
  {
    createAlbum,
    logIn,
    logOut,
    uploadPhoto,
    sendPhotoUrl,
    updateAlbum,
	deleteAlbum,
	selectAlbum,
	syncDoc
  }
)(Admin);
