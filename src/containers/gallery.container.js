import { connect } from 'react-redux';

import Gallery from '../components/Gallery';
import Album from '../components/Album'
import { deletePhoto, syncPhotos, selectAlbum } from '../actions/admin.action';

const mapStateToProps = (state) => ({
	isLogged: state.adminReducer.logInData.logInStatus.isLogged,
	
	photos: state.adminReducer.syncPhoto,
});
export default connect(mapStateToProps, { deletePhoto, syncPhotos, selectAlbum })(Gallery, Album);
