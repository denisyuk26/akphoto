import Messages from '../components/Messages';
import { connect } from 'react-redux';
import { syncMessage, readMessage } from '../actions/message.action';
const mapStateToProps = (state) => ({
	isLogged: state.adminReducer.logInData.logInStatus.isLogged,	
	status: state.adminReducer.logInStatus,
	messages: state.adminReducer.syncMessages.data
});

export default connect(mapStateToProps, { syncMessage, readMessage })(Messages);
