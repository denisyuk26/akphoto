import { combineReducers } from 'redux';
import { handleActions } from 'redux-actions';
import { logIn, logOut } from '../actions/login';

const initialState = {
    status: '',
	token: null,
    message: '',
    isLogged: false
    
};


const logInStatus = handleActions(
	{
		[logIn.REQUEST](state) {
			return {
                ...state,
                status: 'request'
            }
		},
        [logIn.SUCCESS](state, { payload }) {
			return {
                ...state,
                message: '',
                status: 'success',
				token: payload.uid,
				isLogged: true
			};
		},
		[logIn.FAILURE](state, { payload }) {
			return {
                ...state,
                status: 'error',
				message: payload,
				isLogged: false
			};
        },
        [logOut.SUCCESS](state) {
			return {
                ...state,
                status: '',
                token: null,
                message: ''
			};
		},
		
	},
	initialState
);
const logOutStatus = handleActions(
	{   
        [logOut.REQUEST](state) {
			return {
                ...state,
                status: 'request'
            };
		},
		
        [logOut.FAILURE](state, {payload}) {
			return {
                ...state,
                status: 'error',
                message: payload
            };
		}
	},
	initialState
);


export default combineReducers({
	logInStatus,
	logOutStatus,
});
