import { combineReducers } from "redux";
import { handleActions } from "redux-actions";
import {
  createAlbum,
  uploadPhoto,
  selectAlbum,
  percentage,
  deletePhoto,
  syncPhotos,
  updateAlbum,
  deleteAlbum,
  syncDoc
} from "../actions/admin.action";

import {
  syncMessage,
  sendMessage,
  deleteMessage,
  readMessage
} from "../actions/message.action";
import logInData from "./login.reducer";

const photoState = {
  status: "none",
  percentage: 0
};

const syncStatus = handleActions(
  {
    [updateAlbum.REQUEST]() {
      return "req";
    },
    [updateAlbum.SUCCESS]() {
      return "success";
    }
  },
  "none"
);

const albumData = handleActions(
  {
    [syncDoc.SUCCESS](state, { payload }) {
      return payload;
    }
  },
  {}
);

const album = handleActions(
  {
    [createAlbum.REQUEST](state, { payload }) {
      return "";
    },
    [createAlbum.SUCCESS](state, { payload }) {
      return payload;
    },
    [createAlbum.FAILURE](state, { payload }) {
      return payload;
    },
    [selectAlbum.SUCCESS](state, { payload }) {
      return payload;
    },
    [deleteAlbum.SUCCESS]() {
      return "";
    }
  },
  ""
);

const readMessageStatus = handleActions(
  {
    [readMessage.SUCCESS]() {
      return "success";
    }
  },
  "none"
);
const syncPhoto = handleActions(
  {
    [syncPhotos.SUCCESS](state, { payload }) {
      return payload;
    },
    [syncPhotos.FAILURE](state, { error }) {
      return {
        ...state,
        error: error
      };
    },
    [deletePhoto.SUCCESS](state, { payload }) {
      return {
        ...state,
        payload
      };
    }
  },
  []
);

const syncMessages = handleActions(
  {
    [syncMessage.SUCCESS](state, { payload }) {
      return {
        ...state,
        data: [...payload]
      };
    },
    [syncMessage.FAILURE](state, { payload }) {
      return {
        ...state,
        error: true,
        message: "failed sync data"
      };
    }
  },
  []
);

const messageState = {
  data: null,
  error: false,
  message: ""
};

const messageStatus = handleActions(
  {
    [sendMessage.SUCCESS]() {
      return "success";
    },
    [sendMessage.FAILURE]() {
      return "failure";
    },
    [sendMessage.REQUEST]() {
      return "none";
    }
  },
  "none"
);

const messages = handleActions(
  {
    [sendMessage.FAILURE](state, { payload }) {
      return {
        ...state,
        error: true,
        message: "failed send data"
      };
    },
    [sendMessage.SUCCESS](state, { payload }) {
      return {
        ...state,
        data: payload
      };
    },
    [deleteMessage.SUCCESS](state, { payload }) {
      return "status fail";
    },
    [deleteMessage.FAILURE](state, { payload }) {
      return {
        ...state,
        error: true,
        message: "failed delete data"
      };
    }
  },
  messageState
);

const uploadStatus = handleActions(
  {
    [uploadPhoto.REQUEST](state) {
      return {
        percentage: 0,
        status: "request"
      };
    },
    [uploadPhoto.SUCCESS](state) {
      return {
        ...state,
        status: "success"
      };
    },
    [uploadPhoto.FAILURE]() {
      return "failure";
    },
    [percentage.SUCCESS](state, { payload }) {
      return {
        ...state,
        percentage: payload
      };
    }
  },
  photoState
);

export default combineReducers({
  logInData,
  uploadStatus,
  syncPhoto,
  messages,
  messageStatus,
  syncMessages,
  readMessageStatus,
  album,
  syncStatus,
  albumData
});
