import { composeWithDevTools } from 'redux-devtools-extension';
import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import reducer from '../reducers/';

export default function configureStore() {
	const sagaMiddleWare = createSagaMiddleware();
	return {
		...createStore(reducer, composeWithDevTools(applyMiddleware(sagaMiddleWare))),
		runSaga: sagaMiddleWare.run
	};
}

console.log(createStore(reducer).getState());
