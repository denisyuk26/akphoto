import * as firebase from 'firebase';
import ReduxSagaFirebase from 'redux-saga-firebase';

const firebaseConfig = firebase.initializeApp({
	apiKey: 'AIzaSyChESQCt_0cN0bugKvGHVJAVTXex3sItfc',
	authDomain: 'akphoto-860e4.firebaseapp.com',
	databaseURL: 'https://akphoto-860e4.firebaseio.com',
	projectId: 'akphoto-860e4',
	storageBucket: 'akphoto-860e4.appspot.com',
	messagingSenderId: '561058615954'
});
export const auth = firebase.auth();
export const db = firebase.firestore();
export const rsf = new ReduxSagaFirebase(firebaseConfig);
