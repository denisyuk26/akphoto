import { createRoutine } from 'redux-saga-routines';
import { put, call, take, all, fork, takeEvery } from 'redux-saga/effects';
import { rsf } from '../firebase/config';
export const logIn = createRoutine('LOGIN');
export const logOut = createRoutine('LOGOUT');
export const getUser = createRoutine('GETUSER');

function* sagaOnLogIn({ payload: { login, password } }) {
	yield put(logIn.request());
	try {
		const user = yield call(rsf.auth.signInWithEmailAndPassword, login, password);
		yield call(() => localStorage.setItem('authUser', user.user.uid));
		yield call(() => localStorage.setItem('isLogged', 'true'));
		yield put(logIn.success({ user }));
	} catch (error) {
		yield put(logIn.failure(error.message));
	}
}

function* logOutSaga() {
	try {
		yield put(logOut.request())
		yield call(rsf.auth.signOut);
		yield call(() => localStorage.setItem('isLogged', 'false'));
		yield call(() => localStorage.removeItem('authUser'));
		yield put(logOut.success())

	} catch (error) {
		console.log('31', error.message);
		yield put(logOut.failure(error.message))

	}
}

function* logInStatusWatcher() {
	yield put(logIn.request());

	const channel = yield call(rsf.auth.channel);
	while (true) {
		const { user } = yield take(channel);
		if (user) yield put(logIn.success(user));
		else yield put(logIn.failure());
	}
}

export function* loginRootSaga() {
	yield fork(logInStatusWatcher);
	yield all([ takeEvery(logIn.TRIGGER, sagaOnLogIn), takeEvery(logOut.TRIGGER, logOutSaga) ]);
}
