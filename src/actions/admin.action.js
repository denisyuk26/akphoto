import { createRoutine } from "redux-saga-routines";
import { eventChannel } from "redux-saga";
import { put, call, fork, select, takeEvery } from "redux-saga/effects";

import { rsf } from "../firebase/config";

export const percentage = createRoutine("PERCENTAGE");
export const syncPhotos = createRoutine("SYNCPHOTO");
export const uploadPhoto = createRoutine("UPLOAD");
export const deletePhoto = createRoutine("DELETEPHOTO");
export const sendPhotoUrl = createRoutine("SENDPHOTO");

//album
export const syncDoc = createRoutine("SYNCDOC");
export const createAlbum = createRoutine("CREATEALBUM");
export const updateAlbum = createRoutine("SYNCDATA");
export const deleteAlbum = createRoutine("DELETEALBUM");
export const selectAlbum = createRoutine("SELECTALBUM");

let photoName = [];

export function* syncDocSaga(payload) {
  yield fork(rsf.firestore.syncDocument, `photos/${payload}`, {
    successActionCreator: syncDoc.success,
    failureActionCreator: syncDoc.failure,
    transform: doc => {
      console.log(doc);
      return doc.data();
    }
  });
}

export function* syncPhotoSaga() {
  yield fork(rsf.firestore.syncCollection, "photos", {
    successActionCreator: syncPhotos.success,
    failureActionCreator: syncPhotos.failure,
    transform: photoTransformer
  });
}

export function* sagaOnSelectAlbum({ payload }) {
  yield put(uploadPhoto.request());
  const album = yield select(state => state.adminReducer.album);

  console.log(payload);
  if (payload !== "") {
    yield fork(syncDocSaga, payload);
  }
  if(album === '') {
    yield put(percentage.success(0));
  }
  yield put(selectAlbum.success(payload));
}

export function* sagaOnDeleteAlbum({ payload }) {
  
  const album = yield select(state => state.adminReducer.album);
  const albumPhoto = yield select(state => state.adminReducer.albumData.photos);

  try {
    if(albumPhoto !== undefined ){
      for (let key of albumPhoto) {
        if (key !== undefined) {
          yield call(rsf.storage.deleteFile, `photos/${album}/${key.name}`);
        }
      }
    }
    

    yield call(rsf.firestore.deleteDocument, `photos/${album}`);
    yield put(deleteAlbum.success());
    yield call(syncPhotoSaga);
    
  } catch (error) {
    console.log(error);
    

  }
}

export function* sagaOnCreateAlbum({ payload }) {
  yield put(uploadPhoto.request());
  const album = yield call(rsf.firestore.getDocument, `photos/${payload}`);
  yield put(createAlbum.request());
  if (!album.exists) {
    yield call(rsf.firestore.setDocument, `photos/${payload}`, {
      name: payload,
      photos: []
    });
    yield put(createAlbum.success(payload));
  } else {
    yield put(createAlbum.failure(payload));
  }
}

export function* sagaOnUpdateAlbums() {
  let arr = [];
  const albumData = yield select(state => state.adminReducer.albumData);
  const albumName = yield select(state => state.adminReducer.album);
  const uniqueValues = deleteDuplicates(photoName);
  yield put(updateAlbum.request());
  for (let key of uniqueValues) {
    const url = yield call(rsf.storage.getDownloadURL, key.path);
    arr.push({ path: url, name: key.name });
  }
  let updatedAlbum = arr.concat(albumData.photos);
  const filtered = yield deleteDuplicates(updatedAlbum);

  yield call(rsf.firestore.updateDocument, "photos/" + albumName, {
    photos: filtered
  });
  yield put(updateAlbum.success());
  yield call(() => clearGabrage([photoName]));
  yield photoName.splice(0, photoName.length);
  yield put(percentage.success(0));
  yield put(uploadPhoto.request());
}

export function* sagaOnUploadPhoto({ payload }) {
  let task, ch, pct, path;
  const albumName = yield select(state => state.adminReducer.album);
  yield put(uploadPhoto.request());
  payload.map(photo => {
    path = `photos/${albumName}/${photo.name}`;
    photoName.push({ name: photo.name, path: path });
    task = rsf.storage.uploadFile(`photos/${albumName}/${photo.name}`, photo);
  });

  ch = eventChannel(emit => task.on("state_changed", emit));
  yield takeEvery(ch, function* isLoad(snapshot) {
    pct = (snapshot.bytesTransferred * 100) / snapshot.totalBytes;
    yield put(percentage.success(pct));
    yield console.log("success", pct);
  });

  yield task;
  // yield put(percentage.success(pct));
  yield put(uploadPhoto.success());
}

export function* sagaOnDeletePhoto({ payload: { name } }) {
  yield put(deletePhoto.request());
  try {
    yield call(rsf.firestore.deleteDocument, `photos/${name}`);
    yield call(rsf.storage.deleteFile, `photos/${name}`);
    yield put(deletePhoto.success());
    yield call(syncPhotoSaga);
  } catch (error) {
    yield put(deletePhoto.failure(error.message));
  }
}

const clearGabrage = (...args) => {
  return [...args].map(item => {
    if (typeof item === Array) {
      item.splice(0, item.length);
    } else {
      console.log("error 147", item);
    }
  });
};

const photoTransformer = photo => {
  const res = [];
  photo.forEach(doc =>
    res.push({
      id: doc.id,
      ...doc.data()
    })
  );
  return res;
};

const deleteDuplicates = (data, result = []) => {
  const map = new Map();
  let item;
  const filtered = data.filter(item => item !== undefined);
  console.log("filtered data", filtered);
  for (item of filtered) {
    if (!map.has(item.name)) {
      map.set(item.name, true); // set any value to Map
      result.push(item);
    }
  }
  return result;
};
