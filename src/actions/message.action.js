import { createRoutine } from 'redux-saga-routines';
import { put, call, fork, delay } from 'redux-saga/effects';
import * as firebase from 'firebase';

import { rsf } from '../firebase/config';

export const sendMessage = createRoutine('SENDMESSAGE');
export const syncMessage = createRoutine('SYNCMESSAGE');
export const deleteMessage = createRoutine('DELETEMESSAGE');
export const readMessage = createRoutine('READMESSAGE');

export function* sagaOnSendMessage({ payload }) {
	try {
		yield call(rsf.firestore.addDocument, 'messages', {
			contacts: payload.phone,
			message: payload.message,
			name: payload.name,
			createdAt: firebase.firestore.FieldValue.serverTimestamp(),
			isNew: 'true'
		});
		yield put(sendMessage.success());
		yield delay(2000);
		yield put(sendMessage.request());
	} catch (error) {
		console.log(error.message);
	}
}

const messageTransformer = (message) => {
	const res = [];

	message.forEach((doc) =>
		res.push({
			id: doc.id,
			...doc.data()
		})
	);
	return res;
};

export function* sagaOnReadMessage({ payload }) {
	yield call(rsf.firestore.updateDocument, `messages/${payload.id}`, 'isNew', 'false');
	yield put(readMessage.success());
}

export function* sagaOnSyncMessages() {
	console.log('sync');
	yield fork(rsf.firestore.syncCollection, 'messages', {
		successActionCreator: syncMessage.success,
		failureActionCreator: syncMessage.failure,
		transform: messageTransformer
	});
}
